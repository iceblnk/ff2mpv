function onError(error) {
    console.log(`${error}`);
}

function ff2mpv(url) {
    browser.tabs.executeScript({
        code: "video = document.getElementsByTagName('video');video[0].pause();"
    });
    browser.runtime.sendNativeMessage("ff2mpvall", { url: url }).catch(onError);
}

function ff2mpvall(urls) {
    ff2mpv(urls[0]);
}

browser.contextMenus.create({
    id: "ff2mpv",
    title: "Play in MPV",
    contexts: ["link", "image", "video", "audio", "selection", "frame"]
});

browser.contextMenus.create({
    id: "ff2mpvall",
    title: "Play all YT links in MPV",
    contexts: ["page"]
});

browser.contextMenus.onClicked.addListener((info, tab) => {
    switch (info.menuItemId) {
        case "ff2mpv":
            /* These should be mutually exclusive, but,
               if they aren't, this is a reasonable priority.
            */
            url = info.linkUrl || info.srcUrl || info.selectionText || info.frameUrl;
            if (url) ff2mpv(url);
            break;
        case "ff2mpvall":
            const getLinks = browser.tabs.executeScript({
                file: "/get-yt-links.js"
            });
            getLinks.then(ff2mpvall, onError);
            break;
    }
});

browser.browserAction.onClicked.addListener((tab) => {
    ff2mpv(tab.url);
});
